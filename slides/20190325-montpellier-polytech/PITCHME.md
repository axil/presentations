## Introduction to GitLab

---

## In this talk

- Intro to GitLab
- GitLab CI/CD overview
- Small demo


---?include=slides/common/speaker.md
---?include=slides/20190325-montpellier-polytech/intro-git.md
---?include=slides/20190325-montpellier-polytech/gitlab.md
---?include=slides/20190325-montpellier-polytech/gitlab-ci.md
---?include=slides/20190325-montpellier-polytech/gitlab-runner.md
---?include=slides/20190325-montpellier-polytech/demo.md

---

**Thank you!**
</br>
**Questions?**
