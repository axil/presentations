#### Intro to GitLab

GitLab features:

- Is built on Git
- Provides a unified platform to collaborate on code
- Issues, Merge Requests, Code Review
- Built-in Continuous Integration/Deployment
- Single application for the entire software development and operations lifecycle
- <https://about.gitlab.com/what-is-gitlab/>

---

- [Monthly release on the 22nd](https://about.gitlab.com/2019/03/22/gitlab-11-9-released/)
- [New features every month](https://about.gitlab.com/releases/)
- [Open Development](https://gitlab.com/gitlab-org/gitlab-ce)

---

## [Team page](https://about.gitlab.com/company/team/)

- 525 team members
- 129 pets
- 51 countries and regions
- Remote-only

---

- [Handbook](https://about.gitlab.com/handbook/)
- [Direction](https://about.gitlab.com/direction)
- [Strategy](https://about.gitlab.com/strategy)
- [Culture](https://about.gitlab.com/company/culture/)

Note:
Over 2100 pages

---

**Community Edition**
  - Open Source (MIT License)
  - Host it yourself
  - Over 2000 contributors
  - Many applications are built on top of GitLab

Note:

contributors.gitlab.com

+++

Penflip - Authoring site for writing books is built with a fork of GitLab.

![penflip](assets/20190325-montpellier-polytech/penflip.png)

+++

O'Reilly Atlas - Technical publisher O'Reilly Media has build their own frontend on top of a GitLab backend.

![penflip](assets/20190325-montpellier-polytech/atlas-oreilly.png)

+++

<https://www.lafabriquedelaloi.fr> uses GitLab to store French laws in Git and make the lawmaking process visual.

![lafabrique loi](assets/20190325-montpellier-polytech/loi.png)

---

**Enterprise Edition**
  - Build on CE + features for the Enterprise
  - Open core
  - Host it yourself or use GitLab.com

---

**GitLab.com**
  - SaaS running Enterprise Edition
  - Free and unlimited public and private projects

---

A Single application for the entire software development and operations lifecycle

![DevOps lifecycle](assets/20190325-montpellier-polytech/devops-loop-and-spans-small.png)

+++

@snap[north-west]
@size[1.5em](Manage)
@snapend

@snap[west span-60]
@ul[](false)
- Authentication and authorization
- Audit management
- i18n
@ulend
@snapend

@snap[east span-50]
![Manage](assets/20190325-montpellier-polytech/solutions-manage.png)
@snapend

+++

@snap[north-west]
@size[1.5em](Plan)
@snapend

@snap[west span-60]
@ul[](false)
- Issues, labels, milestones
- Issue boards
- Time tracking
- Service Desk
@ulend
@snapend

@snap[east span-50]
![Plan](assets/20190325-montpellier-polytech/solutions-plan.png)
@snapend

+++

@snap[north-west]
@size[1.5em](Create)
@snapend

@snap[west span-60]
@ul[](false)
- Git repository
- Code review
- Wiki
- Search
- Web IDE
@ulend
@snapend

@snap[east span-50]
![Create](assets/20190325-montpellier-polytech/solutions-create.png)
@snapend

+++

@snap[north-west]
@size[1.5em](Verify)
@snapend

@snap[west span-60]
@ul[](false)
- Continuous Integration
- Code Quality
- Performance testing
@ulend
@snapend

@snap[east span-50]
![Verify](assets/20190325-montpellier-polytech/solutions-verify.png)
@snapend

Note:
sitespeed.io
GitLab helps delivery teams fully embrace continuous integration to automate the builds, integration and verification of their code. GitLab’s industry leading CI capabilities enables automated testing, Static Analysis Security Testing, Dynamic Analysis Security testing and code quality analysis to provide fast feedback to developers and testers about the quality of their code. With pipelines that enable concurrent testing and parallel execution, teams quickly get insight about every commit, allowing them to deliver higher quality code faster.

+++

@snap[north-west]
@size[1.5em](Package)
@snapend

@snap[west span-60]
@ul[](false)
- Container Registry
- NPM Registry
- Maven Repository
@ulend
@snapend

@snap[east span-50]
![Package](assets/20190325-montpellier-polytech/solutions-package.png)
@snapend

+++

@snap[north-west]
@size[1.5em](Secure)
@snapend

@snap[west span-60]
@ul[](false)
- Static Application Security Testing (SAST)
- Dynamic Application Security Testing (DAST)
- Secret Scanning
- Container Scanning
- Dependency Scanning
- License Management
@ulend
@snapend

@snap[east span-50]
![Secure](assets/20190325-montpellier-polytech/solutions-secure.png)
@snapend

+++

@snap[north-west]
@size[1.5em](Release)
@snapend

@snap[west span-60]
@ul[](false)
- Continuous Delivery
- Pages
- Review Apps
- Search
- Web IDE
@ulend
@snapend

@snap[east span-50]
![Release](assets/20190325-montpellier-polytech/solutions-release.png)
@snapend

+++

@snap[north-west]
@size[1.5em](Configure)
@snapend

@snap[west span-60]
@ul[](false)
- Auto DevOps
- Kubernetes integration
- ChatOps
- Serverless
@ulend
@snapend

@snap[east span-50]
![Configure](assets/20190325-montpellier-polytech/solutions-configure.png)
@snapend

Note:
Run cloud-agnostic serverless workloads on Kubernestes, deployed via GitLab CI/CD.

+++

@snap[north-west]
@size[1.5em](Monitor)
@snapend

@snap[west]
@ul[](false)
- Metrics (Prometheus)
- Error tracking (Sentry)
- Cluster monitoring
@ulend
@snapend

@snap[east span-50]
![Monitor](assets/20190325-montpellier-polytech/solutions-monitor.png)
@snapend

Note:
Out-of-the-box Kubernetes cluster monitoring

---

![Compare](assets/20190325-montpellier-polytech/devops-tools.png)
