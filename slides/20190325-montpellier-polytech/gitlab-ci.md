### What is Continuous Integration

For every change submitted to an application - even to development branches -
it’s built and tested automatically and continuously, ensuring the introduced
changes pass all tests, guidelines, and code compliance standards you established
for your app.

---

### What can you do with CI on GitLab?

- Build packages/binaries
- Run test suites
- Deploy code once tests pass
- Use for any language and framework
- Use on any platform
- Use for mobile and embedded development

---

**GitLab CI architecture**

![CI architecture](assets/20190325-montpellier-polytech/ci-architecture.png)

---

### Configuration as code

Define a `.gitlab-ci.yml` file per project that contains your tests, allowing
everyone to contribute changes and ensuring every branch gets the tests it needs.

+++

Simple example of a `.gitlab-ci.yml` file

```yaml
image: debian:latest

first job:
  script:
    - touch hello.txt

second job:
  script:
    - echo $CI_JOB_NAME
```

---

### GitLab CI/CD workflow

![Workflow](assets/20190325-montpellier-polytech/gitlab_workflow_example.png)
