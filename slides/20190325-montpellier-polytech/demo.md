### Demo

<https://gitlab.com/jug-montpellier/polytech/ruby-demo>

---

### What we will see

- Setting up _.gitlab-ci.yml_
- Job pipelines
- Define stages
- Caching stuff between stages
- Job artifacts

+++

Use the **CI/CD configuration** button to see the _.gitlab-ci.yml_ file

+++

- Fix the failure
- Go to the **Pipelines** page and see the job running

+++

Maybe add a coverage job
