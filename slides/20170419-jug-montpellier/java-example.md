### Spring boot application demo

---

### What we will see

- Creating and importing a new project
- Setting up _.gitlab-ci.yml_
- Job pipelines
- Define stages
- Caching stuff between stages
- Job artifacts
- Deploy to staging
- Deploy to production

---

1. Create a new account on GitLab.com if you don't already have one
1. Create a new project on GitLab.com
1. Import _https://github.com/spring-projects/spring-petclinic.git_
   using the **Repo by URL** option
1. Make it public
1. Hit **Create project**

+++

Use the **Set up CI** button to create the _.gitlab-ci.yml_ file

![Set up CI](assets/20170419-jug-montpellier/demo-spring-setup-ci.png)

+++

Add the following contents and hit **Commit changes**

```yaml
image: maven:3-jdk-8

maven build:
    script:
    -  mvn clean install
```

+++

Go to the **Pipelines** page and see the job running

![Pipelines page](assets/20170419-jug-montpellier/demo-spring-pipelines.png)

+++

Info how to replicate the demo in the following wiki:
</br>
https://gitlab.com/axil/spring-petclinic/wikis/home
