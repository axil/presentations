### Intro to GitLab Runners

GitLab Runner is the open source project that is used to run your jobs and
send the results back to GitLab. It is used in conjunction with GitLab CI,
the open-source continuous integration service included with GitLab that
coordinates the jobs.

---

### Why would you need your own Runner?

On GitLab.com, you can use shared Runners. But sometimes shared Runners
aren’t a good solution. For example:

- you need to build the software for Windows or macOS, not for Linux
- you are creating some confidential data during the build so you need to
  have them on your own infrastructure

+++

**Supported platforms:**
- i386
- amd64
- arm

**Supported OSes:**
- GNU/Linux
- Windows
- macOS
- FreeBSD

**DEB/RPM packages**

+++

**Choosing the right executor**

On each platform, you can run your jobs in a different way by using the concept 
that we have named _executors_.

![Executors](assets/20170419-jug-montpellier/executors.png)

+++

**Comparing Docker vs Shell**

- **Shell:**
  - Easiest to install and use
  - Uses host environment
  - Less secure - different projects can access each other working directories
- **Docker:**
  - More powerful
  - Separates build environments
  - Use predefined images with required software
  - Works only for GNU/Linux

---

Any questions so far?

---

### Break
