## Introduction to GitLab and GitLab CI

---

## In this talk

- Brief intro to Git and GitLab
- GitLab CI overview
- How GitLab CI works
- Resources to get started

---?include=slides/common/speaker.md
---?include=slides/20170419-jug-montpellier/intro-git.md
---?include=slides/20170419-jug-montpellier/intro-gitlab.md
---?include=slides/20170419-jug-montpellier/gitlab-ci.md
---?include=slides/20170419-jug-montpellier/gitlab-runner.md
---?include=slides/20170419-jug-montpellier/java-example.md
---?include=slides/20170419-jug-montpellier/i2p.md
---?include=slides/20170419-jug-montpellier/resources.md

---

**Thank you!**
</br>
**Questions?**
