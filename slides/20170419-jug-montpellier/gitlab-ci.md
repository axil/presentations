### What is CI

Continuous Integration _is a software development practice where members of a
team integrate their work frequently... verified by an automated build
(including test) to detect integration errors_.

-- Martin Fowler on [Continuous Integration](https://www.martinfowler.com/articles/continuousIntegration.html)

+++

![CI](assets/ci.png)

+++

![CI](assets/cd.png)

+++

![Puppet delivery deployment](assets/20170419-jug-montpellier/puppet_continuous_diagram.gif)

_[source](https://puppet.com/blog/continuous-delivery-vs-continuous-deployment-what-s-diff)_

---

### What can you do with CI on GitLab?

- Build packages
- Run test suites
- Deploy new code once tests pass
- Use for any language and framework
- Use on any platform
- Use for mobile and embedded development
- Use on ARM and Raspberry PI

---

**GitLab CI architecture**

![CI architecture](assets/20170419-jug-montpellier/ci-architecture.png)

---

### Configuration as code

Define a `.gitlab-ci.yml` file per project that contains your tests, allowing
everyone to contribute changes and ensuring every branch gets the tests it needs.

+++

Simple example of a `.gitlab-ci.yml` file

```yaml
image: debian:latest

before_script:
  - apt-get update
  - apt-get install -y tree

print tree:
  script:
    - tree -L 2

exclude PNG images:
  script:
    - tree | grep -v png
```

---

Any questions so far? A demo is coming next!

---

### Demo - Configuring CI

What we will do in this demo:

- Use the project that this presentation was based on
- Prepare a very basic `.gitlab-ci.yml` file
- See what happens after a Git push
