#### A very brief intro to Git

Git concepts:

- **DVCS** - Decentralised Version Control System
- **repository** - the directory which Git tracks for changes
- **commit** - a snapshot of the repository
- **branch** - a unique series of code changes with a unique name. Each
repository can have one or more branches.
