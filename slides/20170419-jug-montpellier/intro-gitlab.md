#### Intro to GitLab

GitLab features:

- Builds on Git
- Provides a unified platform to collaborate on code
- Issues, Merge Requests, Code Review
- Built-in Continuous Integration/Deployment
- <https://about.gitlab.com/features>

---

- Monthly release on the 22nd
- New features
- [Open Development](https://gitlab.com/gitlab-org/gitlab-ce)
- [Direction](https://about.gitlab.com/direction)
- [Strategy](https://about.gitlab.com/strategy)

---

**Community Edition**
  - Open Source (MIT License)
  - On premises
  - Many applications are built on top of GitLab

+++

Penflip - Authoring site for writing books is built with a fork of GitLab.

![penflip](assets/20170419-jug-montpellier/penflip.png)

+++

O'Reilly Atlas - Technical publisher O'Reilly Media has build their own frontend on top of a GitLab backend.

![penflip](assets/20170419-jug-montpellier/atlas-oreilly.png)

+++

<http://www.lafabriquedelaloi.fr> uses GitLab to store French laws in Git and make the lawmaking process visual.

![lafabrique loi](assets/20170419-jug-montpellier/loi.png)

+++

More at https://about.gitlab.com/applications/#built-with-gitlab

---

**Enterprise Edition**
  - Build on CE + features for the Enterprise
  - On premises or GitLab.com
  - Learn more in <https://about.gitlab.com/gitlab-ee>

---

**GitLab.com**
  - SaaS running Enterprise Edition
  - Free and unlimited public and private projects
  - Learn more in <https://about.gitlab.com/gitlab-com>
