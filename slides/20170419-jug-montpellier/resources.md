### Resources

+++

**Git**

- https://try.github.io
- https://git-scm.com/book/en/v2
- https://codewords.recurse.com/issues/two/git-from-the-inside-out

+++

**GitLab**

- [`.gitlab-ci.yml` file for GitLab itself](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/.gitlab-ci.yml)
- GitLab documentation - https://docs.gitlab.com
- Installing a Runner - https://docs.gitlab.com/runner/install/
- Idea to production - https://www.youtube.com/watch?v=PoBaY_rqeKA

+++

**Other**

- https://puppet.com/blog/continuous-delivery-vs-continuous-deployment-what-s-diff
- This presentation source code - https://gitlab.com/axil/presentations/tree/20170419-jug-montpellier
- Spring boot demo - https://gitlab.com/axil/spring-petclinic/wikis/home
