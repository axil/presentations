@snap[west]
@css[bio-about](Technical Writer @GitLab)
<br><br><br>
@fa[terminal fa-3x bio-fa](geek)
<br><br><br>
@css[bio-about](OSS Contributor<br>Arch Linux fanboy)
@snapend

@snap[east bio]
@css[bio-headline](Achilleas Pipinellis)
<br>
@css[bio-byline](@fa[gitlab pad-fa] axil)
@snapend
