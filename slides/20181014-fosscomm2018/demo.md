@snap[north]
Workshop - Bootstrap
@snapend

@ol
- Create an account on https://gitlab.com
- Fork an existing project from https://gitlab.com/pages
- Create pipeline
- Review Pages settings
- Visit site
@olend

---

@snap[north]
Workshop - CI/CD
@snapend

@ol
- Test before deploy
- Add environment
- Deploy
- Rollback
@olend

---

@snap[north]
Workshop - Custom domain
@snapend

@ol
- Add custom domain
- Review changes
- Let's Encrypt
@olend
