---?image=assets/img/bg/gitlab.png&position=center&size=100% 100%

## Host your own site with GitLab Pages

---?include=slides/common/speaker.md
---?include=slides/20181014-fosscomm2018/intro.md
---?include=slides/20181014-fosscomm2018/ssg.md
---?include=slides/20181014-fosscomm2018/pages-intro.md
---?include=slides/20181014-fosscomm2018/demo.md
---?include=slides/20181014-fosscomm2018/resources.md
---?include=slides/common/contact.md

---

**Thank you!**
</br>
**Questions?**
