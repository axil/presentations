### CMS

- Builds each page on-demand.
- Content is fetched from a database and runs through a template engine.

@snap[north]
Static Site Generators
@snapend

@snap[south]
SSG vs CMS
@snapend

---

### SSG

- Generates all the pages once when there's actually changes to the site.
- No moving parts in the deployed website.
- Caching gets much easier, performance goes up and static sites are far more secure.

@snap[north]
Static Site Generators
@snapend

@snap[south]
SSG vs CMS
@snapend

---

### Components of an SSG

A static site is essentially composed of three components:

- the structure (HTML)
- the layout and styles (CSS)
- the behavior (JavaScript)

@snap[north]
Static Site Generators
@snapend

Note:

Συστατικα
- δομη (HTML)
- μορφοποιηση (CSS)
- συμπεριφορά (JS)

Στην τελικη, αυτα ειναι τα αρχεια που θα σερβιρει ο web server στο χρηστη.

---

### Structure of an SSG

A combination of features to make static sites development faster and less repetitive.

1. Environment
1. Template engine
1. Markup language

@snap[north]
Static Site Generators
@snapend

Note:

Η δομή ενος ssg (από τι αποτελειται)
Tα ssg ειναι φτιαγμενα ώστε να μπορούμε να κανουμε αλλαγες γρήγορα και με τις λιγοτερο δυνατες επαναληψεις.

Αυτα οι 3 εννοιες ειναι το ελαχιστο που πρεπει καποιος να γνωριζει, μιας και διαφερουν απο SSG σε SSG.

---

@snap[north]
Static Site Generators
@snapend

### 1. Environment (programming language)

Note:

 περιβαλλον (γλωσσες προγραματισμου)

η γλωσσα στην οποια το ssg γραφτηκε
διαφερει κυριως στην παραμετροποιηση και την επιδοση (ταχυτητα) με την οποια
χτιζεται ενα site. πχ αν αν site περιεχει 1000 σελιδες, με το Hugo (golang) μπορει να χτιστει
σε μερικα δευτερόλεπτα ενω με το jekyll(ruby) μπορει να παρει μερικα λεπτα. και ολο αυτο γιατι
η go ειναι static type ενω η ruby dynamic.

Είναι σημαντικο να διαλεξει κανεις μια γλωσσα που του ειναι οικεια μιας και
μπορει να χρεαιαστει να κανει debug η και να επεκτεινει τις δυνατοτητες ενος SSG μιας και
πολλα υποστηριζουν καποιο plugin system.

---

@snap[north]
Static Site Generators
@snapend

### 2. Template engine

```html
<!DOCTYPE html>
<html lang="en">
	{% include head.html %}
<body>
	{% include header.html %}
	<main class="content">
		{{ content }}
	</main>
	{% include footer.html %}
</body>
</html>
```

@[3,5,9](Include other HTML pages as partials)
@[7](Include the content of a page or a blog post)

Note:

- Καθε template engine εχει το δικο του notation, αυτό ειναι ένα HTML αρχειο το οποιο εχει
  καποια στοιχεια που χρησιμοποιει το SSG. liquid, jekyll, shopify.
  παρατηρηστε τα αγκιστρα, το επι τοις εκατο και τη λεξη include.
- 3 αρχεια για περιεχομενο το οποιο επαναλαμβανεται σε ολο το site (head, header and footer),
και τα περιλαμβανονται σε καθε σελιδα η οποια χρησιμοποιει αυτο το template.
- To μονο που ειναι διαφορετικο ειναι το {{ content }}, το οποιο ειναι το περιεχομενο που
γινεται include απο καποιο αλλο αρχειο συνηθως μια markup γλωσσα οπως θα δουμε παρακατω.
Εν τελει όλα τα αρχεια θα μετατραπουν σε κανονικες HTML σελιδες. Αυτη η διαδικασια ονομαζεται
build, λεμε οτι χτιζουμε ενα site.

πλεονεκτηματα απο το να γραφει καποιος σκετη HTML:
- ελαχιστοποιηση τυπογραφικων λαθων καθως τα αρχεια ειναι επαναχρησιμοποιουμενα μεσω templates, εχουν μικρο μεγεθος και μπορουν να διαβαστουν ευκολα
- αποφυγη επαναληψης: καθε block που χρειαζεται να επαναληφθει μπορουμε να το περιλαβουμε μια φορα και αυτο θα ειναι διαθεσιμο σε ολο το site.
- Γρηγορο update: αν για παραδειγμα αλλαξουμε κατι στο footer.html θα φανει σε ολες τις σελιδες.

---

@snap[north]
Static Site Generators
@snapend

### 3. Markup language

```rst
Section Header
==============

Subsection Header
-----------------
```

reStructuredText

Note:

- markup language
- lightweight markup language
- ευκολη απλη συνταξη

---

@snap[north]
Static Site Generators
@snapend

### 3. Markup language

```md
# Section Header

## Subsection Header
```

Markdown

Note:

- markup language
- lightweight markup language
- ευκολη απλη συνταξη
