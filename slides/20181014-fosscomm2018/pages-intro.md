## GitLab Pages

![GitLab Pages group on GitLab.com](assets/20181410-fosscomm2018/pages-group.png)

---?image=assets/img/bg/orange.jpg&position=right&size=50% 100%

@snap[west split-screen-heading text-orange span-50]
Publish static websites directly from a repository in GitLab
@snapend

@snap[east text-white span-45]
@ol[split-screen-list](false)
- Use any static website generator or plain HTML
- Create websites for your projects, groups, or user account
- Host on GitLab.com for free, or on your own GitLab instance
- Connect your custom domain(s) and TLS certificates
@olend
@snapend

Note:

- To GitLab Pages υποστηριζει ολα τα ssgs.
- Μπορειτε να χρησιμοποιησετε custom domains αντι για το default username.gitlab.io.
- Μπορειτε να ανεβασετε το δικο σας TLS cert
- Δουλευει ακομα και σε private projects
- Όλα δωρεαν στο GitLab.com (το μονο κοστος ειναι το custom domain και αυτο ειναι προαιρετικο)

---

## GitLab CI/CD

![GitLab CI/CD pipeline graph](assets/20181410-fosscomm2018/ci-cd.png)

---

## GitLab Runner

![GitLab Runner architecture](assets/20181410-fosscomm2018/runner.png)

Note:
O GitLab Runner μιλάει με το GitLab και εκτελει τις εντολες που υπαρχουν στο `.gitlab-ci.yml`.
Ειναι γραμμενος σε Go και τρεχει σε Linux, BSD, macOS, Windows, συσκευες με ARM επεξεργαστες όπως το Raspberry pi.
Στο workshop δε θα στησουμε το δικο μας Runner, θα χρησιμοποιησουμε αυτους που παρεχει δωρεαν το GitLab.com (shared Runners).
Τρεχουν σε linux με docker containers, οποτε αν καποιος-α θελει να κανει development σε καποιο άλλο λειτουργικο συστημα
η αρχιτεκτονικη θα πρεπει να χρησιμοποιησει το δικο του.

---

@quote[The key for building your site with GitLab Pages is the GitLab CI configuration file, called `.gitlab-ci.yml`.]

https://docs.gitlab.com/ee/ci/yaml/

---

```yaml
# https://hub.docker.com/_/ubuntu/
image: ubuntu

job name:
  script:
    - echo "FOSSCOMM 2018"
```

---

Jekyll example

```yaml
image: ruby:2.5

pages:
  script:
    - bundle install
    - bundle exec jekyll build -d public
  artifacts:
    paths:
      - public
  only:
    - master
```

@[1](The Docker image to use)
@[3](The job name must be pages)
@[4-6](Commands to build the site)
@[7-9](Upload content in public as artifacts)
@[10,11](Run this only on master branch)

---

@snap[north]
Publish to GitLab Pages
@snapend

Easy process

1. Commit
1. GitLab CI/CD builds site
1. Site is uploaded to GitLab Pages
