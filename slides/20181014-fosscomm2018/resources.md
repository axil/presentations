## Resources

- https://docs.gitlab.com/ee/user/project/pages/
- https://docs.gitlab.com/ee/ci/yaml/
- https://about.gitlab.com/2016/06/03/ssg-overview-gitlab-pages-part-1-dynamic-x-static/
- https://about.gitlab.com/2016/06/10/ssg-overview-gitlab-pages-part-2/
- https://about.gitlab.com/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/
- https://gitlab.com/pages
- https://github.com/rolodato/gitlab-letsencrypt

