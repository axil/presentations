## Slides from presentations

**2019-03-25 Montpellier Polytech:**
  - https://gitpitch.com/axil/presentations/master?grs=gitlab&t=white&p=slides/20190325-montpellier-polytech
  - https://gitlab.com/axil/presentations/tree/master/slides/20190325-montpellier-polytech

**2018-10-14 Fosscomm 2018:**
  - https://gitpitch.com/axil/presentations/master?grs=gitlab&t=white&p=slides/20181014-fosscomm2018
  - https://gitlab.com/axil/presentations/tree/master/slides/20181014-fosscomm2018
  - https://fosscomm2018.gr/fs2018/talk/9ZBHTS/

**2017-04-19 JUG Montpellier:**
  - https://gitpitch.com/axil/presentations/master?grs=gitlab&t=white&p=slides/20170419-jug-montpellier
  - https://gitlab.com/axil/presentations/tree/master/slides/20170419-jug-montpellier
  - https://www.jug-montpellier.org/events/53
